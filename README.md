# LAMP-SSL-WithReverseProxy

basic docker-compose.yml configuration for LAMP stack
this template include Let's Encrypt(SSL) and using nginx reverse proxy

คอนฟิค docker compose สร้าง `LAMP` stack ใช้ `Apache` เป็น webserver 
มีบริการ SSL (จาก Let's Encrypt) และใช้ nginx เป็น reverse proxy

# config & startup
- สร้าง network `my_webproxy` ก่อน
```
docker network create my_webproxy
```
- เปลี่ยนชื่อโฟลเดอร์ `siteX` เป็นชื่อที่ต้องการ
- เก็บไฟล์ของเวปไว้ที่ siteX/public_html
- ทำให้ครบทุกเวปไซต์ (1website=1folder)
- แต่ละเวปแอปให้ตั้งค่าให้เสร็จ และเรียกใช้ docker-compose ใน folder ของเวปไซต์นั้นตามปกติ
```
VIRTUAL_HOST: ${VIRTUAL_HOST_NAME}
LETSENCRYPT_HOST: ${SSL_HOST_NAME}
```

- รัน docker-compose ที่ nginx_proxy_letsencrypt เป็นขั้นตอนสุดท้ายเพื่อจัดทำ SSL
```
docker-compose up -d
```
ps. กรณีมีเวปไซต์ใหม่ที่เพิ่มเข้ามา  ก็ไม่ต้องทำอะไร แค่ start docker-compose ของเวปใหม่
ก็จะใช้งานได้ทันที เพราะจะมีการ detect ในวง network my_webproxy โดยอัตโนมัติ

`TKVR`
